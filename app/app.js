angular.module('pharmeasyApp', [
  'ui.router',
  'ui.materialize',
  'firebase',
  'pharmeasyApp.services',
  'pharmeasyApp.controllers',
  'pharmeasyApp.filters',
])
.config(function($stateProvider, $urlRouterProvider, $qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
  $urlRouterProvider.otherwise("/");
  $stateProvider
    .state('login', {
      url: "/",
      templateUrl: "templates/login.html",
      controller: "LoginCtrl"
    })
    .state('patient', {
      url: "/patient",
      templateUrl: "templates/patient.html",
      controller: "PatientCtrl"
    })
    .state('doctor', {
      url: "/doctor",
      templateUrl: "templates/doctor.html",
      controller: "DoctorCtrl"
    });
})

.run(function ($rootScope, $state) {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log('User is signed in !');
      firebase.database().ref('users/' + btoa(user.email)).on('value', function(snapshot) {
          var user = snapshot.val();
          console.log(user);
          $rootScope.userData = {
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            uid: user.uid,
            role: user.role
          };
          $state.go('login');
          $state.go(user.role);
      });
    } else {
      console.log('No user is signed in !');
    }
  });

  $rootScope.logout = function() {
    console.log('logout called !');
      firebase.auth().signOut().then(function() {
      console.log('Sign-out successful');
      $state.go('login');
    }, function(error) {
      console.log('error in log out');
    })
  };
});
