describe('Test pharmeasyApp', function(){
    var scope;
    var config = {
      apiKey: "AIzaSyDor01sRgzFq4tSUM_bNRzptfldchXgJro",
      authDomain: "pharmeasy-33ca0.firebaseapp.com",
      databaseURL: "https://pharmeasy-33ca0.firebaseio.com",
      projectId: "pharmeasy-33ca0",
      storageBucket: "pharmeasy-33ca0.appspot.com",
      messagingSenderId: "321497477128"
    };
    firebase.initializeApp(config);

    beforeEach(angular.mock.module('pharmeasyApp'));

    beforeEach(angular.mock.inject(function(_$controller_){
      $controller = _$controller_;
    }));


    describe('Test LoginCtrl', function () {
        beforeEach(angular.mock.inject(function($rootScope, $controller){
            scope = $rootScope.$new();
            $controller('LoginCtrl', {$scope: scope});
        }));

        it('should have loginRole as "Patient"', function(){
            expect(scope.select.loginRole).toBe('Patient');
        });
    });

});
