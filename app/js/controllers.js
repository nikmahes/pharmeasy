angular.module('pharmeasyApp.controllers', [])

.controller('LoginCtrl', function($scope, $rootScope, $firebaseAuth, $state) {
  console.log("in LoginCtrl !");

  $scope.database = firebase.database();
  $scope.select = {
    loginRole:'Patient',
  };

  $scope.signInParams = {
    email: "",
    password: "",
  };

  $scope.commonLogin = function(serviceProvider) {
    console.log('In commonLogin !');
    var provider, clientId, clientSecret;
    if (serviceProvider == 'facebook') {
      console.log('Processing facebook login !');
      provider = firebase.auth.FacebookAuthProvider;
      clientId = '221518794977085';
    } else if (serviceProvider == 'google') {
      console.log('Processing google login !');
      provider = firebase.auth.GoogleAuthProvider;
      clientId = '321497477128-rd4o8tfp5ba8euc7llmhtjj94n4fpkee.apps.googleusercontent.com';
    } else if (serviceProvider == 'twitter') {
      console.log('Processing twitter login !');
      provider = firebase.auth.TwitterAuthProvider;
      clientId = 'AWx3Q4xqk9lapFjtNYY4zVL7c';
      clientSecret = '2EIuByqZN4d0cp2gpvEBvf7biX0kXlDEWi00fpq5DfpVQIcqGP';
    }
    $scope.oauthLogin(provider, clientId, clientSecret);
  };

  $scope.oauthLogin = function(provider, clientId, clientSecret) {
    console.log('In oauthLogin !');
    $firebaseAuth(firebase.auth().signInWithPopup(new provider()).then(function(result) {
      console.log('result');
      console.log(result);
      $rootScope.userData = {
        email: result.user.email,
        displayName:result.user.displayName,
        photoURL: result.user.photoURL,
        uid: result.user.uid,
        role: $scope.select.loginRole.toLocaleLowerCase()
      };
      $scope.updateUserToDB($rootScope.userData);
    }).catch(function(error) {
      alert(error)
      console.log(error);
    }));
  };

  $scope.updateUserToDB = function(user) {
    console.log('in updateUserToDB !');
    console.log(user);
    $scope.database.ref('users/' + btoa(user.email)).set(user);
    //$state.go('doctor');
  };
})

.controller('PatientCtrl', function($scope, $rootScope) {
  console.log("in PatientCtrl !");
  $scope.fileObj = $('#file')[0];
  $scope.files = [];
  $scope.fileToPreview = {};
  $scope.emailToShare = "";

  $scope.database = firebase.database();
  $scope.storageRef = firebase.storage().ref();

  $scope.uploadFile = function() {
    console.log('uploadFile called !');
    var file = $scope.fileObj.files[0];
    var reader = new FileReader();
    var timestamp = Date.now();
    $scope.uploadProgress = 0;
    reader.readAsDataURL(file)
    reader.onload = function() {
      var fileInfo = {
          key: Date.now(),
          name: file.name,
          type: file.type,
          size: Math.round(file.size / 1000)+' kB',
          base64: reader.result,
          file: file,
      }
      //console.log("fileInfo : ", fileInfo);
      var fileName = [$scope.userData.email, fileInfo.name, timestamp].join("#");
      var uploadTask = $scope.storageRef.child($scope.userData.email).child(fileName).put(file);
      uploadTask.on('state_changed', function(snapshot){
          $scope.safeApply(function(){
            $scope.uploadProgress = Math.floor((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          });
          //console.log('Upload is ' + $scope.uploadProgress + '% done');
        }, function(error) {
          console.log("error : ", error);
        }, function() {
        var downloadURL = uploadTask.snapshot.downloadURL;
        console.log('downloadURL : ', downloadURL);
        $scope.addFileToDB(fileName, $scope.userData.email, downloadURL);
      });
    }
  };

  $scope.setFileToPreview = function(file) {
    $scope.fileToPreview = file;
  };

  $scope.fetchFilesFromDB = function(self) {
    console.log('fetchFilesFromDB called !');

    if($scope.files.length != 0) return;

    var fileRef = $scope.database.ref(['files', btoa($scope.userData.email), 'selfFiles'].join("/"));
    fileRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var fileObj = snapshot.val();
        fileObj = fileObj == null ? {} : fileObj;
        var fileKeys = Object.keys(fileObj).map(function (key) {
          return key;
        });
        console.log(fileKeys);
        $scope.files = [];
        fileKeys.forEach(function(v){
          var fileArr = atob(v).split('#');
          var sharedWith = fileObj[v].sharedWith;
          sharedWith = sharedWith == "" ? [] :  sharedWith.split(',');

          var requestedBy = fileObj[v].requestedBy;
          requestedBy = requestedBy == "" ? [] :  requestedBy.split(',');

           var file = {
            id: v,
            name: fileArr[1],
            when: fileArr[2],
            url: fileObj[v].url,
            sharedWith: sharedWith,
            requestedBy: requestedBy,
            self: true
          };
          $scope.files.push(file);
        });
      });
    });
  };

  $scope.shareWithUser = function(file) {
    if (!$scope.emailToShare) {
      Materialize.toast('Please enter a valid email to share !', 4000, 'rounded');
      return
    }
    file.sharedWith.push($scope.emailToShare);
    var sharedWith = file.sharedWith.join(',');
    var requestedBy = file.requestedBy.join(',');
    $scope.database.ref(['files', btoa($scope.userData.email), 'selfFiles', file.id].join("/")).set({url: file.url, sharedWith:sharedWith, requestedBy: requestedBy});
    $scope.database.ref(['files', btoa($scope.emailToShare), 'sharedFiles', file.id].join("/")).set(file.url);
    $scope.emailToShare = "";
  };

  $scope.removeAccess = function(file, user) {
    console.log("removeAccess : ", user);
    file.sharedWith = file.sharedWith.filter(function(v){ return v != user});
    file.requestedBy.push(user);
    var sharedWith = file.sharedWith.join(',');
    var requestedBy = file.requestedBy.join(',');
    $scope.database.ref(['files', btoa($scope.userData.email), 'selfFiles', file.id].join("/")).set({url: file.url, sharedWith:sharedWith, requestedBy: requestedBy});
    $scope.database.ref(['files', btoa(user), 'sharedFiles', file.id].join("/")).set("");
  };

  $scope.grantAccess = function(file, user) {
    console.log("grantAccess : ", user);
    file.requestedBy = file.requestedBy.filter(function(v){ return v != user});
    file.sharedWith.push(user);
    var sharedWith = file.sharedWith.join(',');
    var requestedBy = file.requestedBy.join(',');
    $scope.database.ref(['files', btoa($scope.userData.email), 'selfFiles', file.id].join("/")).set({url: file.url, sharedWith:sharedWith, requestedBy: requestedBy});
    $scope.database.ref(['files', btoa(user), 'sharedFiles', file.id].join("/")).set(file.url);
  };

  $scope.addFileToDB = function(fileName, owner, downloadURL) {
    var fileArr = fileName.split('#');
    var base64FileName = btoa(fileName);
    $scope.database.ref(['files', btoa(owner), 'selfFiles', base64FileName].join("/")).set({url:downloadURL, sharedWith: "", requestedBy: ""});
    //$scope.database.ref(['users', btoa($scope.userData.email), 'selfFiles', base64FileName].join("/")).set({url:downloadURL, sharedWith: "", requestedBy: ""});
  };

  $scope.openFileDialog = function() {
    console.log('openFileDialog called !');
    $scope.fileObj.dispatchEvent(new MouseEvent("click"));
    $scope.fileObj.addEventListener("change", $scope.uploadFile);
  };

  $scope.getTime = function(timestamp) {
    console.log('safeApply called !');
    return moment(parseInt(timestamp)).fromNow();
  };

  $scope.safeApply = function(fn) {
    console.log('safeApply called !');
    if (this.$root) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
          if (fn && (typeof (fn) === 'function')) {
            fn();
          }
      } else {
          this.$apply(fn);
      }
    }
  };
})

.controller('DoctorCtrl', function($scope, $rootScope) {
  console.log("in DoctorCtrl !");

  $scope.files = [];
  $scope.fileToPreview = {};
  $scope.emailToSearch = "";


  $scope.database = firebase.database();

  $scope.fetchFilesFromDB = function() {
    console.log('fetchFilesFromDB called !');

    if($scope.files.length != 0) return;

    var fileRef = $scope.database.ref(['files', btoa($scope.userData.email), 'sharedFiles'].join("/"));
    fileRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var fileObj = snapshot.val();
        fileObj = fileObj == null ? {} : fileObj;
        var fileKeys = Object.keys(fileObj).map(function (key) {
          return key;
        });
        console.log(fileKeys);
        $scope.files = [];

        fileKeys.forEach(function(v){
          var fileArr = atob(v).split('#');
           var file = {
            id: v,
            owner: fileArr[0],
            name: fileArr[1],
            when: fileArr[2],
            url: fileObj[v],
            self: false
          };
          $scope.files.push(file);
        });
      });
    });
  };

  $scope.searchUserFiles = function() {
    console.log("searchUserFiles : ", $scope.emailToSearch);
    if ($scope.emailToSearch.trim() == $scope.userData.email) {
      Materialize.toast('It is little wired to request own files !', 4000, 'rounded bottom');
      return
    } else if (!$scope.emailToSearch.trim()) {
      Materialize.toast('Please enter a valid email to search !', 4000, 'rounded');
      return
    }
    var fileRef = $scope.database.ref(['files', btoa($scope.emailToSearch), 'selfFiles'].join("/"));
    fileRef.on('value', function(snapshot) {
      $scope.safeApply(function(){
        var fileObj = snapshot.val();
        fileObj = fileObj == null ? {} : fileObj;
        var fileKeys = Object.keys(fileObj).map(function (key) {
          return key;
        });
        //console.log(fileObj);
        $scope.searchFiles = [];

        fileKeys.forEach(function(v){
          var fileArr = atob(v).split('#');
          var shared = fileObj[v].sharedWith.split(',').filter(function(v){return v == $scope.userData.email});
          var requested = fileObj[v].requestedBy.split(',').filter(function(v){return v == $scope.userData.email});

          var sharedWith = fileObj[v].sharedWith;
          sharedWith = sharedWith == "" ? [] :  sharedWith.split(',');

          var requestedBy = fileObj[v].requestedBy;
          requestedBy = requestedBy == "" ? [] :  requestedBy.split(',');

          var file = {
            id: v,
            owner: fileArr[0],
            name: fileArr[1],
            when: fileArr[2],
            shared: shared[0] == $scope.userData.email,
            requested: requested[0] == $scope.userData.email,
            sharedWith: sharedWith,
            requestedBy: requestedBy,
            url: fileObj[v].url
          };
          $scope.searchFiles.push(file);
        });

        console.log('$scope.searchFiles : ', $scope.searchFiles);
        if ($scope.searchFiles.length == 0) {
          Materialize.toast('There are no files available for this email !', 4000, 'rounded');
        }
      });
    });
  };

  $scope.requestAccess = function(file) {
    console.log("requestAccess ! ");
    file.requestedBy.push($scope.userData.email);
    var sharedWith = file.sharedWith.join(',');
    var requestedBy = file.requestedBy.join(',');
    $scope.database.ref(['files', btoa(file.owner), 'selfFiles', file.id].join("/")).set({url: file.url, sharedWith:sharedWith, requestedBy: requestedBy});
    $scope.database.ref(['files', btoa($scope.userData.email), 'sharedFiles', file.id].join("/")).set("");
  };

  $scope.setFileToPreview = function(file) {
    $scope.fileToPreview = file;
  };

  $scope.getTime = function(timestamp) {
    console.log('safeApply called !');
    return moment(parseInt(timestamp)).fromNow();
  };

  $scope.safeApply = function(fn) {
    console.log('safeApply called !');
    if (this.$root) {
      var phase = this.$root.$$phase;
      if (phase == '$apply' || phase == '$digest') {
          if (fn && (typeof (fn) === 'function')) {
            fn();
          }
      } else {
          this.$apply(fn);
      }
    }
  };
});
