angular.module('pharmeasyApp.filters', []).filter('trustThisUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);