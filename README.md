# About

An serverless application where the User’s data can be shared only if they approve it. There are 3 types of users/roles:
1.    Patient/User
2.    Doctor
3.    Pharmacist

The Patient has medical records and prescriptions. If a doctor asks for a patient’s prescription, the patient has to approve it. Same goes with the Pharmacist, if the pharmacist wants to view the patient’s prescription, the patient has to approve it.

TechStack - AngularJS, MaterializeCSS and Firebase RealtimeDB.

# Local Installation and Execution

1. Install dependencies with npm 
    $ npm install

2. Run Application
    $ npm start

3. Open http://localhost:8080 in browser

4. Test Application
    $ karma start

# Todo(s)

1. ~~Create a Project Base (Node, Express, Gulp, AngularJS, AngularFire, MaterializeCSS)~~
2. Add Test Runner & Write Test Scenarios (Karma, Jasmine)
3. ~~Create a Firebase Project and Integrate Social Login (Google / Twitter) using Firebse Auth~~
4. ~~Create Patient, Doctor and Pharmacist Login Screens and Verify Person as Patient, Doctor or Pharmacist~~

5. Patient/User Features :
• ~~Upload a  medical records or prescriptions~~
• ~~View self medical records or prescriptions~~
• ~~Approve requests to Share his/her medical records or prescriptions~~

6. Doctor Features :
• ~~Request for Patient's medical records or prescriptions~~
• ~~View approved medical records or prescriptions~~

6. Pharmacist Features :
• Request for Patient's medical records or prescriptions
• View approved medical records or prescriptions

7. Scope of Imprevements :
• Code Refactor : Move repetitive code in Services
• Create directives for some Feature
• ~~Add Support for PDF~~
• Add Support for DOC & TXT files

# Demo

https://pharmeasy-33ca0.firebaseapp.com

# Author

Nikhil Maheshwari (http://www.nikhilmaheshwari.com)