// Karma configuration
// Generated on Mon May 29 2017 14:07:09 GMT+0530 (India Standard Time)

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
        './node_modules/angular/angular.js',
        './node_modules/angular-mocks/angular-mocks.js',
        './node_modules/angular-resource/angular-resource.js',
        './node_modules/angular-ui-router/release/angular-ui-router.js',
        './node_modules/angular-materialize/src/angular-materialize.js',
        './node_modules/firebase/firebase.js',
        './node_modules/angularfire/dist/angularfire.js',
        './node_modules/moment/moment.js',
        // './app/**/*.js',
        './app/js/filters.js',
        './app/js/services.js',
        './app/js/controllers.js',
        './app/app.js',
        './app/app.test.js'
    ],
    exclude: [
    ],
    preprocessors: {
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}
